import * as webpackMerge from 'webpack-merge';

const commonConfig = {
  mode: 'production',
  optimization: {
		minimize: false
  },

  resolve: {
    extensions: ['.jsx', '.js']
  },
  module: {
    rules: [{
      test: /\.tsx$|\.ts$/,
      loader: 'awesome-typescript-loader',
      options: {
        useBabel: true,
        babelOptions: {
          babelrc: false,
          presets: ['react']
        }
      },
    }],
  },

  watch: isLocal()
};

const serverConfig = webpackMerge(commonConfig, {
  entry: `${__dirname}/src/server/index.ts`,
  output: {
    path: __dirname,
    filename: 'dist/server.js',
  },

  target: 'node',
  node: {
    process: true,
    // Need to polyfill to get the 'true' directory and file name
    // otherwise, these will be based on the bundled files.
    __dirname: true,
    __filename: true,
  }
});

const clientConfig = webpackMerge(commonConfig, {
  target: 'web',
  entry: `${__dirname}/src/client/stripe/stripe.tsx`,
  output: {
    path: __dirname,
    filename: 'dist/stripe.js',
  },

  externals: {
    'react': 'React',
    'react-dom': 'ReactDOM',
    'axios': 'axios'
  }
});

export default [
  clientConfig
];
