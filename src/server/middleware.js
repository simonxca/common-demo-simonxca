import {getRand} from '../common/utils';

const asyncMiddleware = fn => (req, res, next) => {
  req.on('close', () => {
    console.log(getRand());
    console.log('Client aborted the request');
  });
  Promise.resolve(fn(req, res, next)).catch(next);
};

export default asyncMiddleware;
