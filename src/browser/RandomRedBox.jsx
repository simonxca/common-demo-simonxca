import React from 'react';
import {getRand} from '../common/utils';

class RandomRedBox extends React.Component {
  render() {
    return (
      <div stlye={{
        width: 200,
        height: 200,
        backgroundColor: 'red',
        color: 'white',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        {getRand()}
      </div>
    );
  }
}

export default RandomRedBox;
